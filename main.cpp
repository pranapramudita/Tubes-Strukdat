#include <iostream>
#include "customer.h"
#include "product.h"
#include "relasi.h"
#include "infotype.h"
#include "menu.h"

#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <cstring>
#include "conio.h"


using namespace std;

int main(){
    List_customer P1;
    List_product P2;
    List_relasi R;
    List_relasi H;

    createList_customer(P1);
    createList_product(P2);
    createList_relasi(R);
    createList_relasi(H);

    insertCustomer(allocate_c("jepri"),P1);
    insertCustomer(allocate_c("ucup"),P1);
    insertCustomer(allocate_c("pepen"),P1);
    insertCustomer(allocate_c("kamono"),P1);

    insertLast(P2,allocate_p("Nike"));
    insertLast(P2,allocate_p("Adidas"));
    insertLast(P2,allocate_p("Vans"));
    insertLast(P2,allocate_p("Bata"));
    insertLast(P2,allocate_p("Converse"));

    insertRating(findElm("pepen",P1),findElm("Nike",P2),R,H,5);
    insertRating(findElm("pepen",P1),findElm("Adidas",P2),R,H,5);
    insertRating(findElm("pepen",P1),findElm("Vans",P2),R,H,5);
    insertRating(findElm("pepen",P1),findElm("Bata",P2),R,H,5);
    insertRating(findElm("pepen",P1),findElm("Converse",P2),R,H,1);
    insertRating(findElm("jepri",P1),findElm("Nike",P2),R,H,3);
    insertRating(findElm("jepri",P1),findElm("Adidas",P2),R,H,4);
    insertRating(findElm("jepri",P1),findElm("Vans",P2),R,H,5);
    insertRating(findElm("jepri",P1),findElm("Bata",P2),R,H,2);
    insertRating(findElm("kamono",P1),findElm("Nike",P2),R,H,5);
    insertRating(findElm("kamono",P1),findElm("Adidas",P2),R,H,2);
    insertRating(findElm("kamono",P1),findElm("Vans",P2),R,H,3);
    insertRating(findElm("kamono",P1),findElm("Bata",P2),R,H,1);
    insertRating(findElm("ucup",P1),findElm("Nike",P2),R,H,2);
    insertRating(findElm("ucup",P1),findElm("Adidas",P2),R,H,1);
    insertRating(findElm("ucup",P1),findElm("Vans",P2),R,H,3);
    insertRating(findElm("ucup",P1),findElm("Bata",P2),R,H,0);
    insertRating(findElm("ucup",P1),findElm("Converse",P2),R,H,5);

    int x=0;
    while(x!=4){
        system("cls");
        cout<<"TUBES: RATE THE PRODUCT"<<endl<<endl;
        cout<<"Home"<<endl;
        cout<<"1. Administrator"<<endl;
        cout<<"2. Customer"<<endl;
        cout<<"3. Non Login User"<<endl;
        cout<<"4. Quit"<<endl<<endl;
        cout<<"input: ";
        cin>>x;
        switch(x){
            case 1:{
                int y=0;
                while(y!=8){
                    system("cls");
                    cout<<"LOGIN: Administrator"<<endl<<endl;
                    cout<<"Menu"<<endl;
                    cout<<"1. Input Product"<<endl;
                    cout<<"2. Update Product"<<endl;
                    cout<<"3. Delete Product"<<endl;
                    cout<<"4. View Data Product"<<endl;
                    cout<<"5. View Average and Rate Product"<<endl;
                    cout<<"6. View Detail Rate Customer"<<endl;
                    cout<<"7. Delete Customer"<<endl;
                    cout<<"8. Back"<<endl<<endl;
                    cout<<"input: ";
                    cin>>y;
                    switch(y){
                        case 1:{
                            system("cls");
                            infotype_p2 a;
                            cout<<"Masukan Nama Produk: ";
                            cin>>a;
                            if(findElm(a,P2)){
                                cout<<"Produk sudah terdaftar"<<endl;
                                getchar();
                                getchar();
                            }else{
                                insertLast(P2,allocate_p(a));
                            }
                            break;
                        }
                        case 2:{
                            system("cls");
                            infotype_p2 c,b;
                            address_p p;
                            printInfo_p(P2);
                            cout<<"Masukan Nama Produk yang ingin diupdate: ";
                            cin>>c;
                            p=findElm(c,P2);
                            if(p==NULL){
                                cout<<"Produk belum terdaftar"<<endl;
                                getchar();
                                getchar();
                            }else if(p!=NULL){
                                cout<<"Masukan Nama Produk baru: ";
                                cin>>b;
                                info(p)=b;
                            }
                            break;
                        }
                        case 3:{
                            system("cls");
                            infotype_p2 c;
                            printInfo_p(P2);
                            cout<<"Masukan Nama Produk yang ingin dihapus: ";
                            cin>>c;
                            if(not findElm(c,P2)){
                                cout<<"Produk belum terdaftar"<<endl;
                                getchar();
                                getchar();
                            }else{
                                deleteProduct(findElm(c,P2),P2);
                            }
                            break;
                        }
                        case 4:{
                            system("cls");
                            printInfo_p(P2);
                            getchar();
                            getchar();
                            break;
                        }
                        case 5:{
                            system("cls");
                            rateProd(P2,R);
                            getchar();
                            getchar();
                            break;
                        }
                        case 6:{
                            system("cls");
                            printInfo_r(R);
                            getchar();
                            getchar();
                            break;
                        }
                        case 7:{
                            system("cls");
                            infotype_p1 c;
                            printInfo_c(P1);
                            cout<<"Masukan Nama Produk yang ingin dihapus: ";
                            cin>>c;
                            if(not findElm(c,P1)){
                                cout<<"Customer belum terdaftar"<<endl;
                                getchar();
                                getchar();
                            }else{
                                deleteCustomer(findElm(c,P1),P1);
                            }
                            break;
                        }
                        case 8:{
                            break;
                        }
                        default:{
                            cout<<"Salah "<<endl;
                            getchar();
                            getchar();
                            break;
                        }
                    }
                }
                break;
            }
            case 2:{
                system("cls");
                infotype_p1 c;
                address_c p=NULL;
                cout<<"LOGIN CUSTOMER"<<endl;
                cout<<"Masukan Username: ";
                cin>>c;
                p=findElm(c,P1);
                if(p==NULL){
                    cout<<"User dengan username "<<c<<" belum terdaftar"<<endl;
                    getchar();
                    getchar();
                    break;
                }else if((p!=NULL)&&(info(p)==c)){
                    int z=0;
                    while(z!=5){
                        system("cls");
                        cout<<"LOGIN: Customer:"<<info(p)<<endl<<endl;
                        cout<<"Menu"<<endl;
                        cout<<"1. Input Rate"<<endl;
                        cout<<"2. Delete Rate"<<endl;
                        cout<<"3. View History Product Rate"<<endl;
                        cout<<"4. Update Account"<<endl;
                        cout<<"5. Back"<<endl<<endl;
                        cout<<"input: ";
                        cin>>z;
                        switch(z){
                            case 1:{
                                system("cls");
                                infotype_p2 n;
                                printInfo_p(P2);
                                cout<<"Masukan nama produk yang ingin dirating: ";
                                cin>>n;
                                if(findElm(n,P2)){
                                    if(findCustomerProd(info(p),n,R)){
                                        cout<<"Sudah pernah rate"<<endl;
                                        getchar();
                                        getchar();
                                    }else{
                                        int x;
                                        do{
                                            cout<<"Masukan rating: ";
                                            cin>>x;
                                        }while((x>5)||(x<=0));
                                        insertRating(p,findElm(n,P2),R,H,x);
                                    }
                                }else{
                                    cout<<"Produk belum terdaftar"<<endl;
                                    getchar();
                                    getchar();
                                }
                                break;
                            }
                            case 2:{
                                system("cls");
                                string n;
                                printInfoAkun_r(R,p);
                                cout<<"Masukan nama produk yang ingin dihapus ratenya: ";
                                cin>>n;
                                if(not findCustomerProd(info(p),n,R)){
                                    cout<<"Produk belum terdaftar"<<endl;
                                    getchar();
                                    getchar();
                                }
                                else if(findCustomerProd(info(p),n,R)){
                                    address_r q;
                                    if(findCustomerProd(info(p),n,R)==first(R)){
                                        cout<<"first"<<endl;
                                        getchar();
                                        getchar();
                                        deleteFirst(R,q);
                                    }else{
                                        cout<<"else"<<endl;
                                        getchar();
                                        getchar();
                                        deleteAfter(R,findNextProd(findCustomerProd(info(p),n,R),R),q);
                                    }
                                }
                                break;
                            }
                            case 3:{
                                system("cls");
                                printInfoAkun_r(H,p);
                                getchar();
                                getchar();
                                break;
                            }
                            case 4:{
                                system("cls");
                                infotype_p1 b;
                                cout<<"Masukan Username baru: ";
                                cin>>b;
                                info(p)=b;
                                break;
                            }
                            case 5:{
                                break;
                            }
                            default:{
                                cout<<"Salah "<<endl;
                                getchar();
                                getchar();
                                break;
                            }
                        }
                    }
                }
                break;
            }
            case 3:{
                int o=0;
                while(o!=5){
                    system("cls");
                    cout<<"LOGIN: Non Login User"<<endl<<endl;
                    cout<<"Menu"<<endl;
                    cout<<"1. Sign Up Customer"<<endl;
                    cout<<"2. View Top-10 Rate Product"<<endl;
                    cout<<"3. View Product"<<endl;
                    cout<<"4. View User"<<endl;
                    cout<<"5. Back"<<endl<<endl;
                    cout<<"input: ";
                    cin>>o;
                    switch(o){
                        case 1:{
                            system("cls");
                            infotype_p1 a;
                            cout<<"Masukan username: ";
                            cin>>a;
                            if(findElm(a,P1)){
                                cout<<"User sudah terdaftar"<<endl;
                                getchar();
                                getchar();
                            }else{
                                insertCustomer(allocate_c(a),P1);
                            }
                            break;
                        }
                        case 2:{
                            system("cls");
                            rateEveryProd(P2,R);
                            getchar();
                            getchar();
                            break;
                        }
                        case 3:{
                            system("cls");
                            printInfo_p(P2);
                            getchar();
                            getchar();
                            break;
                        }
                        case 4:{
                            system("cls");
                            printInfo_c(P1);
                            getchar();
                            getchar();
                            break;
                        }
                        case 5:{
                            break;
                        }
                        default:{
                            cout<<"Salah "<<endl;
                            getchar();
                            getchar();
                            break;
                        }
                    }
                }
                break;
            }
            case 4:{
                break;
            }
            default:{
                cout<<"Salah "<<endl;
                getchar();
                getchar();
                break;
            }
        }
    }
}
