#ifndef INFOTYPE_H_INCLUDED
#define INFOTYPE_H_INCLUDED
#include <iostream>

using namespace std;

typedef string infotype_p1;

typedef string infotype_p2;

typedef struct elmList_product *address_p;
typedef struct elmlist_customer *address_c;
typedef struct infotype_r{
    address_c customer;
    address_p product;
    int rating;
};


#endif // INFOTYPE_H_INCLUDED
