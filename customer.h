#ifndef CUSTOMER_H_INCLUDED
#define CUSTOMER_H_INCLUDED

#include <iostream>
#include "infotype.h"
#define first(L) L.first
#define next(P) P->next
#define last(L) L.last
#define prev(P) P->prev
#define info(P) P->info

using namespace std;

typedef struct elmlist_customer *address_c;
struct elmlist_customer{
    infotype_p1 info;
    address_c next;
    address_c prev;
};
struct List_customer{
    address_c first;
    address_c last;
};

void createList_customer(List_customer &L);
address_c allocate_c(infotype_p1 X);
void deallocate_c(address_c &P);
void printInfo_c(List_customer L);
void insertFirst(List_customer &L , address_c P);
void insertAfter(List_customer &L, address_c Prec, address_c P);
void insertBefore(List_customer & L , address_c Prec, address_c P);
void insertLast(List_customer &L, address_c P);
void deleteFirst(List_customer &L, address_c &P);
void deleteAfter(List_customer &L, address_c Prec, address_c &P);
void deleteBefore(List_customer &L, address_c Prec, address_c &P);
void deleteLast(List_customer &L, address_c &P);
address_c findElm(infotype_p1 x, List_customer L);
void insertCustomer(address_c p, List_customer &L);
void deleteCustomer(address_c p, List_customer &L);

#endif // CUSTOMER_H_INCLUDED
