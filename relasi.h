#ifndef RELASI_H_INCLUDED
#define RELASI_H_INCLUDED

#include <iostream>
#include "infotype.h"
#define first(L) L.first
#define next(P) P->next
#define info(P) P->info
#include "product.h"

using namespace std;

typedef struct elmlist_relasi *address_r;
struct elmlist_relasi{
    infotype_r info;
    address_r next;
};
struct List_relasi {
    address_r first;
};

struct typedefavg{
    infotype_p2 prod[100];
    int rate[100];
};

void createList_relasi(List_relasi &L);
address_r allocate_r(infotype_r x);
void deallocate_r(address_r &P);
void printInfo_r(List_relasi L);
void insertFirst(address_r P, List_relasi &L);
void insertAfter(List_relasi &L, address_r Prec, address_r P);
void insertLast(List_relasi &L, address_r P);
void deleteFirst(List_relasi &L, address_r &P);
void deleteLast(List_relasi &L, address_r &P);
void deleteAfter(List_relasi &L, address_r Prec, address_r &P);
address_r findProd(string x, List_relasi L);
void insertRating(address_c p, address_p q, List_relasi &L, List_relasi &K, int x);
void rateEveryProd(List_product p, List_relasi L);
int rateAverageProd(address_p p, List_relasi L);
address_r findCustomerProd(string y, string x, List_relasi L);
int banyakarray(typedefavg x);
address_r findNextProd(address_r x, List_relasi L);
void rateProd(List_product p, List_relasi L);
void printInfoAkun_r(List_relasi L, address_c q);

#endif // RELASI_H_INCLUDED
