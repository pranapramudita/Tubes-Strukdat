#ifndef PRODUCT_H_INCLUDED
#define PRODUCT_H_INCLUDED

#include <iostream>
#include "infotype.h"
#define first(L) L.first
#define next(P) P->next
#define last(L) L.last
#define prev(P) P->prev
#define info(P) P->info

using namespace std;

typedef struct elmList_product *address_p;
struct elmList_product {
    infotype_p2 info;
    address_p next;
    address_p prev;
};
struct List_product{
    address_p first;
    address_p last;
};

void createList_product(List_product &L);
address_p allocate_p(infotype_p2 X);
void deallocate_p(address_p &P);
void printInfo_p(List_product L);
void insertFirst(List_product &L , address_p P);
void insertAfter(List_product &L, address_p Prec, address_p P);
void insertBefore(List_product & L , address_p Prec, address_p P);
void insertLast(List_product &L, address_p P);
void deleteFirst(List_product &L, address_p &P);
void deleteAfter(List_product &L, address_p Prec, address_p &P);
void deleteBefore(List_product &L, address_p Prec, address_p &P);
void deleteLast(List_product &L, address_p &P);
address_p findElm(infotype_p2 x, List_product L);
void deleteProduct(address_p p, List_product &L);

#endif // PRODUCT_H_INCLUDED
